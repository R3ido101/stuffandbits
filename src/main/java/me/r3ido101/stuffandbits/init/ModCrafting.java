package me.r3ido101.stuffandbits.init;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModCrafting {

    public static void register()
    {
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.doubleIron), new Object[] {"II ", "II ", 'I', new ItemStack(Items.IRON_INGOT)});
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.doubleGold), new Object[] {"GG ", "GG ", 'G', new ItemStack(Items.GOLD_INGOT)});
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.doubleDiamond), new Object[] {"DD ", "DD ", 'D', new ItemStack(Items.DIAMOND)});
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.doubleEmerald), new Object[] {"EE ", "EE ", 'E', new ItemStack(Items.EMERALD)});
        GameRegistry.addShapedRecipe(new ItemStack(ModItems.doubleRedstone), new Object[] {"RR ", "RR ", 'R', new ItemStack(Items.REDSTONE)});

    }
}
