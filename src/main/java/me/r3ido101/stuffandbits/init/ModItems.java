package me.r3ido101.stuffandbits.init;


import me.r3ido101.stuffandbits.items.*;
import me.r3ido101.stuffandbits.utils.ModInfo;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems
{
    public static Item doubleIron;
    public static Item doubleGold;
    public static Item doubleDiamond;
    public static Item doubleEmerald;
    public static Item doubleRedstone;


    public static void init()
    {
        doubleIron = new itemDIron();
        doubleGold = new itemDGold();
        doubleDiamond = new itemDDiamond();
        doubleEmerald = new itemDEmerald();
        doubleRedstone = new ItemDRedstone();
    }

    public static void register()
    {
        GameRegistry.register(doubleIron);
        GameRegistry.register(doubleGold);
        GameRegistry.register(doubleDiamond);
        GameRegistry.register(doubleEmerald);
        GameRegistry.register(doubleRedstone);
    }

    public static void registerRenders()
    {
        registerRender(doubleIron);
        registerRender(doubleGold);
        registerRender(doubleDiamond);
        registerRender(doubleEmerald);
        registerRender(doubleRedstone);
    }

    public static void registerRender(Item item)
    {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(ModInfo.modID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }
}
