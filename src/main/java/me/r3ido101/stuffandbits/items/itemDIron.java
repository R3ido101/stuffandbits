package me.r3ido101.stuffandbits.items;

import me.r3ido101.stuffandbits.Main;
import me.r3ido101.stuffandbits.utils.ModInfo;
import me.r3ido101.stuffandbits.utils.sabItems;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class itemDIron extends Item
{
    public itemDIron()
    {
        setUnlocalizedName(sabItems.DOUBLE_ITRON.getUnlocalizedName().substring(5));
        setRegistryName(sabItems.DOUBLE_ITRON.getRegisteryName());
        setCreativeTab(Main.CREATIVE_TAB);

    }
}
