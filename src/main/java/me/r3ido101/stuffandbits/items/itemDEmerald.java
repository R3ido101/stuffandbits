package me.r3ido101.stuffandbits.items;

import me.r3ido101.stuffandbits.Main;
import me.r3ido101.stuffandbits.utils.sabItems;
import net.minecraft.item.Item;

public class itemDEmerald extends Item {

    public itemDEmerald(){
        setUnlocalizedName(sabItems.DOUBLE_EMERALD.getUnlocalizedName().substring(5));
        setRegistryName(sabItems.DOUBLE_EMERALD.getRegisteryName());
        setCreativeTab(Main.CREATIVE_TAB);
    }

}
