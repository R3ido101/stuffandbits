package me.r3ido101.stuffandbits.items;

import me.r3ido101.stuffandbits.Main;
import me.r3ido101.stuffandbits.utils.sabItems;
import net.minecraft.item.Item;

public class itemDGold extends Item {
    public itemDGold()
    {
        setUnlocalizedName(sabItems.DOUBLE_GOLD.getUnlocalizedName().substring(5));
        setRegistryName(sabItems.DOUBLE_GOLD.getRegisteryName());
        setCreativeTab(Main.CREATIVE_TAB);
    }
}
