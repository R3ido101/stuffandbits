package me.r3ido101.stuffandbits.items;


import me.r3ido101.stuffandbits.Main;
import me.r3ido101.stuffandbits.utils.sabItems;
import net.minecraft.item.Item;

public class itemDDiamond extends Item {
    public itemDDiamond(){
        setUnlocalizedName(sabItems.DOUBLE_DIAMOND.getUnlocalizedName().substring(5));
        setRegistryName(sabItems.DOUBLE_DIAMOND.getRegisteryName());
        setCreativeTab(Main.CREATIVE_TAB);
    }
}
