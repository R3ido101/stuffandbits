package me.r3ido101.stuffandbits.proxy;

import me.r3ido101.stuffandbits.init.ModItems;

public class ClientProxy implements CommonProxy {
    @Override
    public void init()
    {
        ModItems.registerRenders();
    }
}
