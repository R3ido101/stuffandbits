package me.r3ido101.stuffandbits.utils;

import me.r3ido101.stuffandbits.init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by Oliver on 25/11/2016.
 */
public class CreativeTab extends CreativeTabs {
    public CreativeTab() {
        super("tabSAB");
    }


    @Override
    public Item getTabIconItem() {
        return ModItems.doubleDiamond;
    }
}
