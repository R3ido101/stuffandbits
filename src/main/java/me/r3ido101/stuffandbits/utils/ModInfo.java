package me.r3ido101.stuffandbits.utils;


public class ModInfo {
    // This is all the info for the mod!

    public static final String modName = "Stuff&Bits";
    public static final String modID = "sab";
    public static final String modVersion = "1.0";
    public static final String allowedVersion = "[1.10.2]";

    public static final String serverProxy = "me.r3ido101.stuffandbits.proxy.ServerProxy";
    public static final String clientProxy = "me.r3ido101.stuffandbits.proxy.ClientProxy";

}
