package me.r3ido101.stuffandbits.utils;


public enum sabItems {
    DOUBLE_ITRON("double_iron", "itemDoubleIron"),
    DOUBLE_GOLD("double_gold", "itemDoubleGold"),
    DOUBLE_DIAMOND("double_Diamond", "itemDoubleDiamond"),
    DOUBLE_EMERALD("double_Emerald", "itemDoubleEmerald"),
    DOUBLE_REDSTONE("Double Redstone","itemDoubleRedstone");

    private String unlocalizedName;
    private String registeryName;


    sabItems(String unlocalizedName, String registeryName)
    {
        this.unlocalizedName = unlocalizedName;
        this.registeryName = registeryName;
    }

    public String getUnlocalizedName()
    {
        return unlocalizedName;
    }

    public String getRegisteryName()
    {
        return registeryName;
    }
}
