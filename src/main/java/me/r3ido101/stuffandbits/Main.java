package me.r3ido101.stuffandbits;

import me.r3ido101.stuffandbits.init.ModItems;
import me.r3ido101.stuffandbits.init.ModCrafting;
import me.r3ido101.stuffandbits.proxy.CommonProxy;
import me.r3ido101.stuffandbits.utils.CreativeTab;
import me.r3ido101.stuffandbits.utils.ModInfo;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModInfo.modID, name = ModInfo.modName, version = ModInfo.modVersion, acceptedMinecraftVersions = ModInfo.allowedVersion)
public class Main
{
    @Mod.Instance
    public static Main instance;

    @SidedProxy(clientSide = ModInfo.clientProxy, serverSide = ModInfo.serverProxy)
    public static CommonProxy proxy;

    public static final CreativeTabs CREATIVE_TAB = new CreativeTab();


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        System.out.println("Stuff&Bits Pre-Initialization");
        ModItems.init();
        ModItems.register();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        System.out.println("Stuff&Bits Initialization");
        proxy.init();
        ModCrafting.register();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        System.out.println("Stuff&Bits Post-Initialization");
    }


}
